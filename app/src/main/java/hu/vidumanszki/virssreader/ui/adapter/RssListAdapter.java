package hu.vidumanszki.virssreader.ui.adapter;


import android.view.View;
import android.widget.TextView;

import hu.vidumanszki.utils.recyclerview_empty.BaseRecyclerViewAdapter;
import hu.vidumanszki.utils.recyclerview_empty.BaseViewHolder;
import hu.vidumanszki.virssreader.R;
import hu.vidumanszki.virssreader.model.RssListObject;

public class RssListAdapter extends BaseRecyclerViewAdapter<RssListAdapter.ViewHolder, RssListObject> {

    public RssListAdapter(RecyclerViewClickListener<RssListObject> clickListener) {
        super(clickListener);
    }

    @Override
    protected int getRowRes() {
        return R.layout.row_rss_list;
    }

    @Override
    protected ViewHolder getViewHolderClass(View itemView, RecyclerViewClickListener<RssListObject> clickListener) {
        return new ViewHolder(itemView, clickListener);
    }

    class ViewHolder extends BaseViewHolder<RssListObject> {

        TextView tvTitle;
        TextView tvLink;

        ViewHolder(View itemView, BaseRecyclerViewAdapter.RecyclerViewClickListener<RssListObject> clickListener) {
            super(itemView, clickListener);

            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvLink = itemView.findViewById(R.id.tvLink);
        }

        @Override
        public void dataDidSet() {
            tvTitle.setText(dataItem.getTitle());
            tvLink.setText(dataItem.getLink());
        }
    }

}
