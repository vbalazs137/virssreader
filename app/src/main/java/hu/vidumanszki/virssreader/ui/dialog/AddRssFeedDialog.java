package hu.vidumanszki.virssreader.ui.dialog;


import android.content.Context;
import android.view.View;
import android.widget.EditText;

import hu.vidumanszki.utils.dialog.CustomDialog;
import hu.vidumanszki.virssreader.R;
import hu.vidumanszki.virssreader.service.AppCallback;
import hu.vidumanszki.virssreader.service.RssListService;

public class AddRssFeedDialog extends CustomDialog {

    private RssListService rssListService;
    private AppCallback<Boolean> appCallback;

    private EditText etUrl;

    public AddRssFeedDialog(Context context,
                            RssListService rssListService,
                            AppCallback<Boolean> appCallback) {
        super(context);

        this.rssListService = rssListService;
        this.appCallback = appCallback;

        setBtnPosVisible(true);
        setBtnPosTextRes(android.R.string.ok);

        setBtnNegVisible(true);
        setBtnNegTextRes(android.R.string.cancel);

        setTitleRes(R.string.dialog_title_rss_url);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.dialog_add_rssfeed;
    }

    @Override
    protected void initUi(View view) {
        etUrl = view.findViewById(R.id.etLink);
    }

    @Override
    protected void btnPosAction() {
        rssListService.addRssUrl(etUrl.getText().toString(), appCallback);
    }
}
