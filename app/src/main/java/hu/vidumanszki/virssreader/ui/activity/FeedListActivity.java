package hu.vidumanszki.virssreader.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.widget.ProgressBar;

import java.util.List;

import hu.vidumanszki.utils.recyclerview_empty.BaseRecyclerViewAdapter;
import hu.vidumanszki.utils.recyclerview_empty.RecyclerViewEmptySupport;
import hu.vidumanszki.virssreader.R;
import hu.vidumanszki.virssreader.model.RssFeedObject;
import hu.vidumanszki.virssreader.model.RssListObject;
import hu.vidumanszki.virssreader.service.AppCallback;
import hu.vidumanszki.virssreader.service.RssListService;
import hu.vidumanszki.virssreader.ui.adapter.RssFeedAdapter;

public class FeedListActivity extends BaseActivity implements
        BaseRecyclerViewAdapter.RecyclerViewClickListener<RssFeedObject>,
        AppCallback<List<RssFeedObject>> {

    static void startFeedListActivity(BaseActivity sourceActivity, RssListObject rssListObject) {
        Intent i = new Intent(sourceActivity, FeedListActivity.class);
        i.putExtra(RssListObject.TAG, rssListObject);
        sourceActivity.startActivity(i);
    }

    private RssListService rssListService;

    private RssFeedAdapter adapter;

    private ProgressBar progressBar;

    private RssListObject rssListObject;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        rssListService = RssListService.getInstance(this);

        super.onCreate(savedInstanceState);
    }

    @Override
    int getContentViewRes() {
        return R.layout.activity_feed_list;
    }

    @Override
    void initUi() {
        rssListObject = (RssListObject) getIntent().getSerializableExtra(RssListObject.TAG);

        progressBar = findViewById(R.id.progressBar);

        initRecyclerView();

        initActionBar();
    }

    private void initRecyclerView() {
        RecyclerViewEmptySupport recyclerViewEmptySupport = findViewById(R.id.recyclerViewRssList);
        adapter = new RssFeedAdapter(this);
        recyclerViewEmptySupport.setAdapter(adapter);
        recyclerViewEmptySupport.setLayoutManager(new LinearLayoutManager(this));

        showProgressBar(progressBar, true);

        rssListService.getRssFeedObjects(rssListObject.getLink(), this);
    }

    private void initActionBar() {
        showHomeButton(true);
        setTitle(rssListObject.getTitle());
    }

    @Override
    public void onItemSelected(RssFeedObject clickedItem) {
        DetailsActivity.startDetailsActivity(this, clickedItem);
    }

    @Override
    public void onDataAvailable(List<RssFeedObject> data, String error) {
        showProgressBar(progressBar, false);

        adapter.setData(data);

        RssListService.errorToast(FeedListActivity.this, error);
    }
}
