package hu.vidumanszki.virssreader.ui.adapter;


import android.view.View;
import android.widget.TextView;

import hu.vidumanszki.utils.recyclerview_empty.BaseRecyclerViewAdapter;
import hu.vidumanszki.utils.recyclerview_empty.BaseViewHolder;
import hu.vidumanszki.virssreader.R;
import hu.vidumanszki.virssreader.model.RssFeedObject;

public class RssFeedAdapter extends BaseRecyclerViewAdapter<RssFeedAdapter.ViewHolder, RssFeedObject> {

    public RssFeedAdapter(RecyclerViewClickListener<RssFeedObject> clickListener) {
        super(clickListener);
    }

    @Override
    protected int getRowRes() {
        return R.layout.row_rss_feed;
    }

    @Override
    protected ViewHolder getViewHolderClass(View itemView, RecyclerViewClickListener<RssFeedObject> clickListener) {
        return new ViewHolder(itemView, clickListener);
    }

    class ViewHolder extends BaseViewHolder<RssFeedObject> {

        TextView tvTitle;

        ViewHolder(View itemView, RecyclerViewClickListener<RssFeedObject> clickListener) {
            super(itemView, clickListener);

            tvTitle = itemView.findViewById(R.id.tvTitle);
        }

        @Override
        public void dataDidSet() {
            tvTitle.setText(dataItem.getTitle());
        }
    }

}
