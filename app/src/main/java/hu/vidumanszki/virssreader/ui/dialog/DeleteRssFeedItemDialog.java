package hu.vidumanszki.virssreader.ui.dialog;


import android.content.Context;

import hu.vidumanszki.utils.dialog.BaseDialog;
import hu.vidumanszki.virssreader.R;
import hu.vidumanszki.virssreader.model.RssListObject;
import hu.vidumanszki.virssreader.service.RssListService;

public class DeleteRssFeedItemDialog extends BaseDialog {

    private RssListService rssListService;
    private RssListObject rssListObject;

    public DeleteRssFeedItemDialog(Context context, RssListService rssListService, RssListObject rssListObject) {
        super(context);

        this.rssListService = rssListService;
        this.rssListObject = rssListObject;

        String titleText =
                context.getString(R.string.dialog_title_delete_rss) +
                        " " + rssListObject.getTitle() + "?";

        setTitle(titleText);

        setBtnPosVisible(true);
        setBtnPosTextRes(android.R.string.yes);

        setBtnNegVisible(true);
        setBtnNegTextRes(android.R.string.no);
    }

    @Override
    protected void btnPosAction() {
        super.btnPosAction();

        rssListService.removeRss(rssListObject);
    }
}
