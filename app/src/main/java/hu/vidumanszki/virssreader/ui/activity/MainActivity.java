package hu.vidumanszki.virssreader.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.ProgressBar;

import java.util.List;

import hu.vidumanszki.utils.dialog.BaseDialog;
import hu.vidumanszki.utils.recyclerview_empty.BaseRecyclerViewAdapter;
import hu.vidumanszki.utils.recyclerview_empty.RecyclerViewEmptySupport;
import hu.vidumanszki.virssreader.R;
import hu.vidumanszki.virssreader.model.RssListObject;
import hu.vidumanszki.virssreader.service.AppCallback;
import hu.vidumanszki.virssreader.service.RssListService;
import hu.vidumanszki.virssreader.ui.adapter.RssListAdapter;
import hu.vidumanszki.virssreader.ui.dialog.AddRssFeedDialog;
import hu.vidumanszki.virssreader.ui.dialog.DeleteRssFeedItemDialog;

public class MainActivity extends BaseActivity implements
        BaseRecyclerViewAdapter.RecyclerViewClickListener<RssListObject>,
        BaseRecyclerViewAdapter.RecyclerViewLongClickListener<RssListObject>,
        View.OnClickListener {

    private RssListAdapter adapter;

    private ProgressBar progressBar;

    private RssListService rssListService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        rssListService = RssListService.getInstance(this);

        super.onCreate(savedInstanceState);
    }

    @Override
    int getContentViewRes() {
        return R.layout.activity_main;
    }

    @Override
    void initUi() {
        progressBar = findViewById(R.id.progressBar);

        initRecyclerView();

        FloatingActionButton btnAdd = findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);
    }

    private void initRecyclerView() {
        RecyclerViewEmptySupport recyclerView = findViewById(R.id.recyclerViewRssFeed);
        adapter = new RssListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter.setLongClickListener(this);

        refreshAdapter();
    }

    private void refreshAdapter() {
        showProgressBar(progressBar, true);

        rssListService.getRssListObjects(loadRssFeedCallback);
    }

    @Override
    public void onItemSelected(RssListObject clickedItem) {
        FeedListActivity.startFeedListActivity(this, clickedItem);
    }

    @Override
    public void onItemLongClick(RssListObject clickedItem) {
        DeleteRssFeedItemDialog dialog = new DeleteRssFeedItemDialog(this, rssListService, clickedItem);
        dialog.setNoticeDialogListener(new BaseDialog.NoticeDialogListener() {
            @Override
            public void onDialogPositiveClick(BaseDialog dialog) {
                refreshAdapter();
            }

            @Override
            public void onDialogNegativeClick(BaseDialog dialog) {
                //nothing here
            }
        });
        dialog.showDialog();
    }

    @Override
    public void onClick(View v) {
        AddRssFeedDialog dialog = new AddRssFeedDialog(this, rssListService, addRssFeedCallback);
        dialog.setNoticeDialogListener(new BaseDialog.NoticeDialogListener() {
            @Override
            public void onDialogPositiveClick(BaseDialog dialog) {
                showProgressBar(progressBar, true);
            }

            @Override
            public void onDialogNegativeClick(BaseDialog dialog) {
                //nothing here
            }
        });
        dialog.showDialog();
    }

    private AppCallback<List<RssListObject>> loadRssFeedCallback = new AppCallback<List<RssListObject>>() {
        @Override
        public void onDataAvailable(List<RssListObject> data, String error) {
            showProgressBar(progressBar, false);

            adapter.setData(data);

            RssListService.errorToast(MainActivity.this, error);
        }
    };

    private AppCallback<Boolean> addRssFeedCallback = new AppCallback<Boolean>() {
        @Override
        public void onDataAvailable(Boolean data, String error) {
            showProgressBar(progressBar, false);

            if (data && error == null) {
                refreshAdapter();
            } else {
                RssListService.errorToast(MainActivity.this, error);
            }
        }
    };
}
