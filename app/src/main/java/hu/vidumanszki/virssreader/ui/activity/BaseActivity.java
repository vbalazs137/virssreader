package hu.vidumanszki.virssreader.ui.activity;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

abstract class BaseActivity extends AppCompatActivity {

    abstract int getContentViewRes();

    abstract void initUi();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(getContentViewRes());

        initUi();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void showHomeButton(boolean isShow) {
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(isShow);
        }
    }

    protected static void showProgressBar(ProgressBar progressBar, boolean isShow) {
        int visibility = isShow ? View.VISIBLE : View.INVISIBLE;

        progressBar.setVisibility(visibility);
    }

}
