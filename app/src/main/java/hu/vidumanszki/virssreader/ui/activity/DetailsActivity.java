package hu.vidumanszki.virssreader.ui.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import hu.vidumanszki.utils.view.BitmapWorkerTask;
import hu.vidumanszki.virssreader.R;
import hu.vidumanszki.virssreader.model.RssFeedObject;

public class DetailsActivity extends BaseActivity {

    static void startDetailsActivity(BaseActivity sourceActivity, RssFeedObject rssFeedObject) {
        Intent i = new Intent(sourceActivity, DetailsActivity.class);
        i.putExtra(RssFeedObject.TAG, rssFeedObject);
        sourceActivity.startActivity(i);
    }

    private RssFeedObject rssFeedObject;

    @Override
    int getContentViewRes() {
        return R.layout.activity_details;
    }

    @Override
    void initUi() {
        rssFeedObject = (RssFeedObject) getIntent().getSerializableExtra(RssFeedObject.TAG);

        ImageView imageView = findViewById(R.id.imageView);
        TextView tvDetails = findViewById(R.id.tvDetails);
        TextView tvLink = findViewById(R.id.tvLink);
        TextView tvTitle = findViewById(R.id.tvTitle);

        tvTitle.setText(rssFeedObject.getTitle());

        if (rssFeedObject.getImageLink() != null) {
            BitmapWorkerTask.setImage(imageView, Uri.parse(rssFeedObject.getImageLink()));
        } else {
            imageView.setVisibility(View.GONE);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tvDetails.setText(Html.fromHtml(rssFeedObject.getDetails(), Html.FROM_HTML_MODE_COMPACT));
        } else {
            tvDetails.setText(Html.fromHtml(rssFeedObject.getDetails()));
        }

        tvLink.setText(rssFeedObject.getLink());

        initFacebookShare();

        initActionBar();
    }

    private void initFacebookShare() {
        Button btnFacebook = findViewById(R.id.btnFacebook);
        final ShareDialog shareDialog = new ShareDialog(this);
        btnFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ShareDialog.canShow(ShareLinkContent.class)) {
                    ShareLinkContent shareLinkContent = new ShareLinkContent.Builder()
                            .setContentUrl(Uri.parse(rssFeedObject.getLink()))
                            .build();


                    shareDialog.show(shareLinkContent);
                }
            }
        });
    }

    private void initActionBar() {
        showHomeButton(true);
        setTitle(rssFeedObject.getTitle());
    }

}
