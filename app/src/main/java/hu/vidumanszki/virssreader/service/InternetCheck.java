package hu.vidumanszki.virssreader.service;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import hu.vidumanszki.virssreader.app.ViRssReader;

public class InternetCheck {

    public static boolean isAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) ViRssReader.baseContest.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
