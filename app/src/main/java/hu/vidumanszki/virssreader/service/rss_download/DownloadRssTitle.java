package hu.vidumanszki.virssreader.service.rss_download;


import hu.vidumanszki.virssreader.model.RssListObject;
import hu.vidumanszki.virssreader.service.AppCallback;
import hu.vidumanszki.virssreader.service.RssListService;
import hu.vidumanszki.virssreader.service.rss_parse.ParseRssTitle;

class DownloadRssTitle extends DownloadRssHelper<RssListObject> {

    private String title;

    DownloadRssTitle(AppCallback<RssListObject> appCallback, String link) {
        super(appCallback, link);
    }

    @Override
    public boolean handleBackground() {
        try {
            title = new ParseRssTitle().parseRss(getInputStream());

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void onPostExecute(boolean result) {
        RssListObject rssListObject = null;

        if (result) {
            if (title != null) {
                rssListObject = new RssListObject(title, link);
            }
        } else {
            if (error == null) {
                error = RssListService.ERROR_NO_VALID_URL;
            }
        }

        appCallback.onDataAvailable(rssListObject, error);
    }
}
