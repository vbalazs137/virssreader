package hu.vidumanszki.virssreader.service.rss_parse;


import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;

import java.io.InputStream;

public abstract class ParseRssHelper<T> {

    T parsedData;

    protected abstract void handleXmlPullParser(XmlPullParser xmlPullParser) throws Exception;

    public T parseRss(InputStream inputStream) throws Exception {
        try {
            XmlPullParser xmlPullParser = Xml.newPullParser();
            xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            xmlPullParser.setInput(inputStream, null);

            xmlPullParser.nextTag();

            handleXmlPullParser(xmlPullParser);

            return parsedData;
        } finally {
            inputStream.close();
        }
    }

}
