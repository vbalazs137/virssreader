package hu.vidumanszki.virssreader.service.rss_download;


import java.util.List;

import hu.vidumanszki.virssreader.model.RssFeedObject;
import hu.vidumanszki.virssreader.service.AppCallback;
import hu.vidumanszki.virssreader.service.RssListService;
import hu.vidumanszki.virssreader.service.rss_parse.ParseRssFeed;

class DownloadRssFeed extends DownloadRssHelper<List<RssFeedObject>> {

    private List<RssFeedObject> rssFeedObjects;

    DownloadRssFeed(AppCallback<List<RssFeedObject>> appCallback, String link) {
        super(appCallback, link);
    }

    @Override
    public boolean handleBackground() {
        try {
            rssFeedObjects = new ParseRssFeed().parseRss(getInputStream());

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void onPostExecute(boolean result) {
        if (!result) {
            if (error == null) {
                error = RssListService.ERROR_NO_VALID_URL;
            }
        }

        appCallback.onDataAvailable(rssFeedObjects, error);
    }
}
