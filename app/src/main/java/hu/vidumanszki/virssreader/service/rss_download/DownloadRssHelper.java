package hu.vidumanszki.virssreader.service.rss_download;


import android.text.TextUtils;

import java.io.InputStream;
import java.net.URL;

import hu.vidumanszki.virssreader.service.AppCallback;
import hu.vidumanszki.virssreader.service.InternetCheck;
import hu.vidumanszki.virssreader.service.RssListService;

public abstract class DownloadRssHelper<T> {

    protected AppCallback<T> appCallback;
    protected String link;

    protected String error;

    public DownloadRssHelper(AppCallback<T> appCallback, String link) {
        this.appCallback = appCallback;
        this.link = link;
    }

    public boolean doInBackground() {
        boolean isNetworkAvailable = InternetCheck.isAvailable();

        if (isNetworkAvailable) {
            return handleBackground();
        } else {
            error = RssListService.ERROR_NO_INTERNET;
            return false;
        }
    }

    public abstract boolean handleBackground();

    public abstract void onPostExecute(boolean result);

    InputStream getInputStream() throws Exception {
        if (TextUtils.isEmpty(link))
            return null;

        if (!link.startsWith("http://") && !link.startsWith("https://")) {
            link = "http://" + link;
        }

        URL url = new URL(link);

        return url.openConnection().getInputStream();
    }

}
