package hu.vidumanszki.virssreader.service.rss_parse;


import org.xmlpull.v1.XmlPullParser;

import java.util.ArrayList;
import java.util.List;

import hu.vidumanszki.virssreader.model.RssFeedObject;

public class ParseRssFeed extends ParseRssHelper<List<RssFeedObject>> {

    private String title = null;
    private String link = null;
    private String details = null;
    private String imageLink = null;

    @Override
    protected void handleXmlPullParser(XmlPullParser xmlPullParser) throws Exception {
        parsedData = new ArrayList<>();

        while (xmlPullParser.next() != XmlPullParser.END_DOCUMENT) {
            int eventType = xmlPullParser.getEventType();

            String name = xmlPullParser.getName();
            if (name == null) {
                continue;
            }

            if (eventType == XmlPullParser.END_TAG) {
                if (name.equalsIgnoreCase("item")) {

                    if (title != null && link != null && details != null) {
                        RssFeedObject item = new RssFeedObject(title, link, details, imageLink);
                        parsedData.add(item);

                        title = null;
                        link = null;
                        details = null;
                        imageLink = null;
                    }
                }
                continue;
            }

            if (eventType == XmlPullParser.START_TAG) {
                if (name.equalsIgnoreCase("item")) {
                    continue;
                }
            }

            String result = "";

            if (xmlPullParser.next() == XmlPullParser.TEXT) {
                result = xmlPullParser.getText();
                xmlPullParser.nextTag();
            }

            if (name.equalsIgnoreCase("title")) {
                title = result;
            } else if (name.equalsIgnoreCase("link")) {
                link = result;
            } else if (name.equalsIgnoreCase("description")) {
                details = result;
            } else if (name.equalsIgnoreCase("media:thumbnail")) {
                imageLink = xmlPullParser.getAttributeValue(null, "url");
            }
        }
    }
}
