package hu.vidumanszki.virssreader.service;


public interface AppCallback<T> {

    void onDataAvailable(T data, String error);

}
