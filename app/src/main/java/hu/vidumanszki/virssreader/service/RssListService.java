package hu.vidumanszki.virssreader.service;


import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import hu.vidumanszki.virssreader.R;
import hu.vidumanszki.virssreader.model.RssFeedObject;
import hu.vidumanszki.virssreader.model.RssListObject;
import hu.vidumanszki.virssreader.service.rss_download.DownloadRss;

public class RssListService {

    private static final String DB_RSS = "db_rss";

    public static final String ERROR_NO_INTERNET = "error_no_internet";
    public static final String ERROR_NO_VALID_URL = "error_no_valid_url";

    private static RssListService instance;

    private Context context;

    public static RssListService getInstance(Context context) {
        if (instance == null) {
            instance = new RssListService(context);
        }

        return instance;
    }

    private RssListService(Context context) {
        this.context = context;
    }

    public void addRssUrl(String url, final AppCallback<Boolean> appCallback) {
        DownloadRss.getRssFeedTitle(url, new AppCallback<RssListObject>() {
            @Override
            public void onDataAvailable(RssListObject data, String error) {
                boolean isDataValid = data != null;
                if (isDataValid) {
                    addRssListObject(data);
                }
                appCallback.onDataAvailable(isDataValid, error);
            }
        });
    }

    public void removeRss(RssListObject rssListObject) {
        SharedPreferences.Editor et = getSharedPreferences().edit();
        et.remove(rssListObject.getLink());
        et.commit();
    }

    public void getRssListObjects(AppCallback<List<RssListObject>> appCallback) {
        List<RssListObject> items = new ArrayList<>();

        Map<String, ?> feeds = getSharedPreferences().getAll();

        if (feeds != null) {
            for (Map.Entry<String, ?> entry : feeds.entrySet()) {
                RssListObject rssListObject = new RssListObject(
                        entry.getValue().toString(), entry.getKey());

                items.add(rssListObject);
            }
        }

        if (appCallback != null) {
            appCallback.onDataAvailable(items, null);
        }
    }

    public void getRssFeedObjects(String url, AppCallback<List<RssFeedObject>> appCallback) {
        DownloadRss.getRssFeedList(url, appCallback);
    }

    public static void errorToast(Context context, String error) {
        if (error != null) {
            int errorRes = 0;

            switch (error) {
                case ERROR_NO_INTERNET:
                    errorRes = R.string.error_no_internet;
                    break;
                case ERROR_NO_VALID_URL:
                    errorRes = R.string.error_not_valid_url;
                    break;
            }

            if (errorRes != 0) {
                Toast.makeText(context, context.getString(errorRes), Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void addRssListObject(RssListObject rssListObject) {
        SharedPreferences.Editor et = getSharedPreferences().edit();
        et.putString(rssListObject.getLink(), rssListObject.getTitle());
        et.commit();
    }

    private SharedPreferences getSharedPreferences() {
        return context.getSharedPreferences(DB_RSS, Context.MODE_PRIVATE);
    }

}
