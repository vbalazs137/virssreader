package hu.vidumanszki.virssreader.service.rss_parse;


import org.xmlpull.v1.XmlPullParser;

public class ParseRssTitle extends ParseRssHelper<String> {

    @Override
    protected void handleXmlPullParser(XmlPullParser xmlPullParser) throws Exception {

        while (xmlPullParser.next() != XmlPullParser.END_DOCUMENT) {

            int eventType = xmlPullParser.getEventType();

            String name = xmlPullParser.getName();
            if (name == null) {
                continue;
            }

            if (eventType == XmlPullParser.END_TAG) {
                continue;
            }

            if (eventType == XmlPullParser.START_TAG) {
                if (name.equalsIgnoreCase("channel")) {
                    continue;
                }
            }

            String result = "";
            if (xmlPullParser.next() == XmlPullParser.TEXT) {
                result = xmlPullParser.getText();
                xmlPullParser.nextTag();
            }

            if (name.equalsIgnoreCase("title")) {
                parsedData = result;
            }

            if (parsedData != null) {
                break;
            }

        }

    }
}
