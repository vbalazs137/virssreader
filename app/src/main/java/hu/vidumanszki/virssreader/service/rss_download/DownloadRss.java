package hu.vidumanszki.virssreader.service.rss_download;


import android.os.AsyncTask;

import java.util.List;

import hu.vidumanszki.virssreader.model.RssFeedObject;
import hu.vidumanszki.virssreader.model.RssListObject;
import hu.vidumanszki.virssreader.service.AppCallback;

public class DownloadRss {

    public static void getRssFeedTitle(String url, AppCallback<RssListObject> appCallback) {
        DownloadRssTitle downloadRssTitle = new DownloadRssTitle(appCallback, url);
        new DownloadRssTask<>(downloadRssTitle).execute();
    }

    public static void getRssFeedList(String url, AppCallback<List<RssFeedObject>> appCallback) {
        DownloadRssFeed downloadRssFeed = new DownloadRssFeed(appCallback, url);
        new DownloadRssTask<>(downloadRssFeed).execute();
    }

    private static class DownloadRssTask<T> extends AsyncTask<Void, Void, Boolean> {

        private DownloadRssHelper<T> downloadRssHelper;

        DownloadRssTask(DownloadRssHelper<T> downloadRssHelper) {
            this.downloadRssHelper = downloadRssHelper;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            return downloadRssHelper.doInBackground();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            downloadRssHelper.onPostExecute(result);
        }
    }
}
