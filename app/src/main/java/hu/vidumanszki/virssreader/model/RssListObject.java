package hu.vidumanszki.virssreader.model;


import java.io.Serializable;

public class RssListObject implements Serializable {

    public static final String TAG = "tag_rss_list_item";

    private String title;
    private String link;

    public RssListObject(String title, String link) {
        this.title = title;
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public String getLink() {
        return link;
    }

}
