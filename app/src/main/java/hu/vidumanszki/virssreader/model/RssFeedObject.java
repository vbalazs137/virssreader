package hu.vidumanszki.virssreader.model;


import java.io.Serializable;

public class RssFeedObject implements Serializable {

    public static final String TAG = "tag_rss_feed_item";

    private String title;
    private String link;
    private String details;
    private String imageLink;

    public RssFeedObject(String title, String link, String details, String imageLink) {
        this.title = title;
        this.link = link;
        this.details = details;
        this.imageLink = imageLink;
    }

    public String getTitle() {
        return title;
    }

    public String getLink() {
        return link;
    }

    public String getDetails() {
        return details;
    }

    public String getImageLink() {
        return imageLink;
    }

}
